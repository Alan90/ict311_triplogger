package alanm.triplogger;

import android.media.Image;

import java.util.Date;
import java.util.UUID;

/**
 * Created by alanm on 9/09/2016.
 *
 * This class holds the model for each trip.
 */

public class Trip {

    private UUID mId;
    private String mTitle;
    private Date mDate;
    private String mTripType; //Must be either Work/Personal/Commute
    private Double mTripLat;
    private Double mTripLong;
    private String mLocationDisp;
    private int mTripMins;
    private int mTripHrs;
    private int mTripDays;
    private String mComment;

    public Trip() {
        this(UUID.randomUUID());
    }

    public Trip(UUID id){
        mId = id;
        mDate = new Date();
    }

    public UUID getId() { return mId; }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getTripType() {
        return mTripType;
    }

    public void setTripType(String tripType) {
        mTripType = tripType;
    }

    public Double getTripLat() {
        return mTripLat;
    }

    public void setTripLat(Double tripLat) {
        mTripLat = tripLat;
    }

    public Double getTripLong() {
        return mTripLong;
    }

    public void setTripLong(Double tripLong) {
        mTripLong = tripLong;
    }

    public String getLocationDisp() {
        return mLocationDisp;
    }

    public void setLocationDisp(String locationDisp) {
        mLocationDisp = locationDisp;
    }

    public int getTripMins() {
        return mTripMins;
    }

    public void setTripMins(int tripMins) {
        mTripMins = tripMins;
    }

    public int getTripDays() {
        return mTripDays;
    }

    public void setTripDays(int tripDays) {
        mTripDays = tripDays;
    }

    public int getTripHrs() {
        return mTripHrs;
    }

    public void setTripHrs(int tripHrs) {
        mTripHrs = tripHrs;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

}
