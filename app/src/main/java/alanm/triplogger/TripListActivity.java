package alanm.triplogger;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
/**
 * Created by alanm on 9/09/2016.
 *
 * Activity to host the main list of trips
 */
public class TripListActivity extends SingleFragmentActivity{

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, TripListActivity.class);
        return intent;
    }

    /**
     * Method : override - createFragment()
     * Description : creates trip list fragment
     * @return TripListFragment
     */
    @Override
    protected Fragment createFragment() { return new TripListFragment(); }
}
