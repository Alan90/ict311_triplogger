package alanm.triplogger;

import java.util.UUID;

/**
 * Created by alanm on 6/10/2016.
 *
 * This class holds the model for the user profile
 */
public class Profile {
    private UUID mUUID;
    private String mStudentName = "Alan";
    private String mStudentId = "999999";
    private String mEmail= "hgjk@hotmail.com";
    private String mGender = "fgh";
    private String mComment = "This is a comment  ";

    public Profile() {
        this(UUID.randomUUID());
    }

    public Profile(UUID id){
        mUUID = id;
    }

    public UUID getId() { return mUUID; }

    public String getStudentName() {
        return mStudentName;
    }

    public void setStudentName(String studentName) {
        mStudentName = studentName;
    }

    public String getStudentId() {
        return mStudentId;
    }

    public void setStudentId(String studentId) {
        mStudentId = studentId;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

}
