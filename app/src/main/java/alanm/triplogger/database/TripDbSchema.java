package alanm.triplogger.database;

/**
 * Created by alanm on 11/09/2016.
 *
 * This class defines the string constants needed to describe the moving pieces of table def
 */
public class TripDbSchema {

    /**
     * Method : TripTable()
     * Description: Defines the trip table
     */
    public static final class TripTable{
        //Table names:
        public static final String NAME = "trips";

        //Describes trip columns:
        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String TRIPTYPE = "triptype"; //Must be either Work/Personal/Commute
            public static final String TRIPLAT = "triplat";
            public static final String TRIPLONG = "triplong";
            public static final String TRIPLOCDISP = "triplocdisp";
            public static final String TRIPDAYS = "tripdays";
            public static final String TRIPHRS = "triphrs";
            public static final String TRIPMINS = "tripmins";
            public static final String TRIPCOMMENT = "tripcomment";
        }
    }
}
