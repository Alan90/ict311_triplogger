package alanm.triplogger.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "profileBase.db";

    public ProfileBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * Method : override - onCreate()
     * Description : creates a new database
     * @param db (SQLiteDatabase) db - the database
     */
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table " + ProfileDbSchema.ProfileTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                ProfileDbSchema.ProfileTable.Cols.STUDENTNAME + ", " +
                ProfileDbSchema.ProfileTable.Cols.STUDENTID + ", " +
                ProfileDbSchema.ProfileTable.Cols.EMAIL + ", " +
                ProfileDbSchema.ProfileTable.Cols.GENDER + ", " +
                ProfileDbSchema.ProfileTable.Cols.COMMENT +
                ")"
        );

        // Inserts single profile entry into database with default values:
        db.execSQL("insert into " + ProfileDbSchema.ProfileTable.NAME + " values (null, 'John Doe', '999999', 'johndoe@example.com', 'Male', 'This course was awesome!!! :)')");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // Intentionally left blank for now
    }
}