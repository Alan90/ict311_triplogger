package alanm.triplogger.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import alanm.triplogger.Profile;

/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileCursorWrapper extends CursorWrapper {
    public ProfileCursorWrapper(Cursor cursor){
        super(cursor);
    }

    /**
     * Method : getProfile()
     * Description : pulls out relevant column data
     * @return (Profile) profile - details
     */
    public Profile getProfile() {
        String studentName = getString(getColumnIndex(ProfileDbSchema.ProfileTable.Cols.STUDENTNAME));
        String studentID = getString(getColumnIndex(ProfileDbSchema.ProfileTable.Cols.STUDENTID));
        String email = getString(getColumnIndex(ProfileDbSchema.ProfileTable.Cols.EMAIL));
        String gender = getString(getColumnIndex(ProfileDbSchema.ProfileTable.Cols.GENDER));
        String comment = getString(getColumnIndex(ProfileDbSchema.ProfileTable.Cols.COMMENT));

        //create profile from column data
        Profile profile = new Profile();
        profile.setStudentName(studentName);
        profile.setStudentId(studentID);
        profile.setEmail(email);
        profile.setGender(gender);
        profile.setComment(comment);

        return profile;
    }
}
