package alanm.triplogger.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import java.util.Date;
import java.util.UUID;
import alanm.triplogger.Trip;

/**
 * Created by alanm on 11/09/2016.
 *
 */
public class TripCursorWrapper extends CursorWrapper{
    public TripCursorWrapper(Cursor cursor){
        super(cursor);
    }

    /**
     * Method : getTrip()
     * Description : pulls out relevant column data
     * @return (Trip) trip - details
     */
    public Trip getTrip() {
        String uuidString = getString(getColumnIndex(TripDbSchema.TripTable.Cols.UUID));
        String title = getString(getColumnIndex(TripDbSchema.TripTable.Cols.TITLE));
        long date = getLong(getColumnIndex(TripDbSchema.TripTable.Cols.DATE));
        String tripType = getString(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPTYPE));
        Double tripLat = getDouble(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPLAT));
        Double tripLong = getDouble(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPLONG));
        String tripLocDisp = getString(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPLOCDISP));
        int tripDays = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPDAYS));
        int tripHrs = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPHRS));
        int tripMins = getInt(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPMINS));
        String tripComment = getString(getColumnIndex(TripDbSchema.TripTable.Cols.TRIPCOMMENT));

        Trip trip = new Trip(UUID.fromString(uuidString));
        trip.setTitle(title);
        trip.setDate(new Date(date));
        trip.setTripType(tripType);
        trip.setTripLat(tripLat);
        trip.setTripLong(tripLong);
        trip.setLocationDisp(tripLocDisp);
        trip.setTripDays(tripDays);
        trip.setTripHrs(tripHrs);
        trip.setTripMins(tripMins);
        trip.setComment(tripComment);

        return trip;
    }
}
