package alanm.triplogger.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import alanm.triplogger.database.TripDbSchema.TripTable;

/**
 * Created by alanm on 11/09/2016.
 *
 */
public class TripBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "tripBase.db";

    public TripBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * Method : override - onCreate()
     * Description : creates a new database
     * @param db (SQLiteDatabase) db - the database
     */
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table " + TripDbSchema.TripTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                TripTable.Cols.UUID + ", " +
                TripTable.Cols.TITLE + ", " +
                TripTable.Cols.DATE + ", " +
                TripTable.Cols.TRIPTYPE + ", " +
                TripTable.Cols.TRIPLAT + ", " +
                TripTable.Cols.TRIPLONG + ", " +
                TripTable.Cols.TRIPLOCDISP + ", " +
                TripTable.Cols.TRIPDAYS + ", " +
                TripTable.Cols.TRIPHRS + ", " +
                TripTable.Cols.TRIPMINS + ", " +
                TripTable.Cols.TRIPCOMMENT +
                ")"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // Left intentionally blank for now
    }
}
