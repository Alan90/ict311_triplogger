package alanm.triplogger.database;

/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileDbSchema {
    /**
     * Method : ProfileTable()
     * Description: Defines the profile table
     */
    public static final class ProfileTable{
        //Table names:
        public static final String NAME = "profile";

        //Describes profile columns:
        public static final class Cols {
            public static final String STUDENTNAME = "studentname";
            public static final String STUDENTID = "studentid";
            public static final String EMAIL = "email";
            public static final String GENDER = "gender";
            public static final String COMMENT = "comment";
        }
    }
}