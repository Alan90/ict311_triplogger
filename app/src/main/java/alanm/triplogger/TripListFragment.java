package alanm.triplogger;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;

/**
 * Created by alanm on 30/08/2016.
 */
public class TripListFragment extends Fragment {

    private RecyclerView mTripRecyclerView;
    private TripAdapter mAdapter;
    private Button mNewTripButton;
    private Button mSettingsButton;
    private Profile mProfile;
    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    public static final int MY_PERMISSIONS_READ_EXTERNAL_STORAGE = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_trip_list, container, false);

        mTripRecyclerView = (RecyclerView) view.findViewById(R.id.trip_recycler_view);
        mTripRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mNewTripButton = (Button) view.findViewById(R.id.new_trip);
        mNewTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Trip trip = new Trip();
                TripRepo.get(getActivity()).addTrip(trip);
                Intent intent = TripActivity.newIntent(getActivity(), trip.getId());
                startActivity(intent);
            }
        });

        mSettingsButton = (Button) view.findViewById(R.id.settings_button);
        mSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                ProfileRepo profileRepo = ProfileRepo.get(getActivity());
                Profile profile = profileRepo.getProfile();
                ProfileRepo.get(getActivity()).addProfile(profile);
                Intent intent = ProfileActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });



        updateUI();

        //REQUEST DANGEROUS PERMISSIONS
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
        }

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_READ_EXTERNAL_STORAGE);
        }

        return view;
    }



    @Override
    public void onResume(){
        super.onResume();
        updateUI();
    }

    private void updateUI(){
        TripRepo tripRepo = TripRepo.get(getActivity());
        List<Trip> trips = tripRepo.getTrips();

        if (mAdapter == null){
            mAdapter = new TripAdapter(trips);
            mTripRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setTrips(trips);
            mAdapter.notifyDataSetChanged();
        }

    }

    //Viewholder inner class:
    private class TripHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mLatitudeTextView;
        private TextView mLongitudeTextView;
        private Trip mTrip;

        public TripHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);

            mTitleTextView = (TextView)
                    itemView.findViewById(R.id.list_item_trip_title_text_view);
            mDateTextView = (TextView)
                    itemView.findViewById(R.id.list_item_trip_date_text_view);
            mLatitudeTextView = (TextView)
                    itemView.findViewById(R.id.list_item_trip_latitude_text_view);
            mLongitudeTextView = (TextView)
                    itemView.findViewById(R.id.list_item_trip_longitude_text_view);
        }

        @Override
        public void onClick(View v){
            Intent intent = TripActivity.newIntent(getActivity(), mTrip.getId());
            startActivity(intent);
        }

        public void bindTrip(Trip trip){
            mTrip = trip;
            mTitleTextView.setText(mTrip.getTitle());
            mDateTextView.setText(mTrip.getDate().toString());
            mLatitudeTextView.setText(mTrip.getTripLat().toString());
            mLongitudeTextView.setText(mTrip.getTripLong().toString());
        }
    }

    //Adapter inner class:
    private class TripAdapter extends RecyclerView.Adapter<TripHolder>{

        private List<Trip> mTrips;

        public TripAdapter(List<Trip> trips){
            mTrips = trips;
        }

        @Override
        public TripHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_trip, parent, false);
            return new TripHolder(view);
        }

        @Override
        public void onBindViewHolder(TripHolder holder, int position){
            Trip trip = mTrips.get(position);
            holder.bindTrip(trip);
        }

        @Override
        public int getItemCount(){
            return mTrips.size();
        }

        public void setTrips(List<Trip> trips){
            mTrips = trips;
        }
    }
}
