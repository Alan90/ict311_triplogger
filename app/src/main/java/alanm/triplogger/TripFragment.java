package alanm.triplogger;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import java.io.File;
import java.util.UUID;

/**
 * Created by alanm on 9/09/2016.
 *
 * This is the fragment in which an existing trip can be edited.
 * Controller level.
 *
 */

public class TripFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String ARG_TRIP_ID = "trip_id";
    private static final int REQUEST_PHOTO =2;

    private Trip mTrip;
    private GoogleApiClient mGoogleApiClient; // Client to access play services (eg. location)


    //----------------------------------------This will need to change:
    private EditText mTitleField;
    private Button mDateButton;
    private Button mViewLocation;
    private RadioButton mTripTypeWork;
    private RadioButton mTripTypeCommute;
    private RadioButton mTripTypePersonal;
    private RadioGroup mTripTypeGroup;
    private Button mDestinationButton;
    private Button mDeleteCancelButton;
    private Button mSaveButton;
    private Location mCurrentLocation;
    private NumberPicker mMinsPicker;
    private NumberPicker mHrsPicker;
    private NumberPicker mDaysPicker;
    private EditText mCommentField;
    private File mPhotoFile;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;


    //----------------------------------------

    /**
     * Method : newInstance(UUID)
     * Description : instantiates a new TripFragment instance
     * @param TripId : ID of the trip being viewed
     * @return : (TripFragment) fragment
     */
    public static TripFragment newInstance(UUID TripId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP_ID, TripId);

        TripFragment fragment = new TripFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Intentionally left blank
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Intentionally left blank
    }

    /**
     * Method : updatePhotoView()
     * Description : Loads Bitmap into the imageview
     */
    private void updatePhotoView() {
        if (mPhotoFile == null || !mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }

    /**
     * Method : override - onCreate()
     * Description : initialises fragment
     * @param savedInstanceState : state of the saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        UUID tripId = (UUID) getArguments().getSerializable(ARG_TRIP_ID);
        mTrip = TripRepo.get(getActivity()).getTrip(tripId);
        mPhotoFile = TripRepo.get(getActivity()).getPhotoFile(mTrip);
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /**
     * Method : override - onPause()
     * Description : re-initialises fragment
     */
    @Override
    public void onPause() {
        super.onPause();

        TripRepo.get(getActivity())
                .updateTrip(mTrip);
    }

    /**
     * Method: override - onCreateView()
     * Description : used to create ui elements and generate the UI file
     *
     * @param inflater : use to inflate layout
     * @param container : container to hold the fragment
     * @param savedInstanceState : saved instance state
     * @return : (View) v
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_trip, container,false);

        //--- Set & wire delete/cancel button:
        mDeleteCancelButton = (Button) v.findViewById(R.id.deletecancel_button);
        if(mTrip.getLocationDisp() == null){
            mDeleteCancelButton.setText("Cancel");
        } else {
            mDeleteCancelButton.setText("Delete");
        }
        mDeleteCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripRepo.get(getActivity()).deleteTrip(mTrip);
                Intent intent = TripListActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });

        //--- Wire save button:
        mSaveButton = (Button) v.findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripRepo.get(getActivity()).updateTrip(mTrip);
                Intent intent = TripListActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });

        //--- Trip title:
        mTitleField = (EditText)v.findViewById(R.id.trip_title);
        mTitleField.setText(mTrip.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes trip title to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTrip.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });

        //---  date:
        mDateButton = (Button)v.findViewById(R.id.trip_date);
        mDateButton.setText(mTrip.getDate().toString());
        mDateButton.setEnabled(false);

        //--- trip type:
        mTripTypeWork = (RadioButton) v.findViewById(R.id.triptype_work);
        mTripTypeCommute = (RadioButton) v.findViewById(R.id.triptype_commute);
        mTripTypePersonal = (RadioButton) v.findViewById(R.id.triptype_personal);
        mTripTypeGroup = (RadioGroup) v.findViewById(R.id.triptype_group);

        String tripTypeCurrent = mTrip.getTripType();

        if(tripTypeCurrent != null){
            switch(tripTypeCurrent){
                case "Work":
                    mTripTypeWork.setChecked(true);
                    break;
                case "Commute":
                    mTripTypeCommute.setChecked(true);
                    break;
                case "Personal":
                    mTripTypePersonal.setChecked(true);
                    break;
            }
        }
        else{
            //Set default value:
            mTrip.setTripType("Work");
            mTripTypeWork.setChecked(true);
        }

        mTripTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.triptype_work:
                        mTrip.setTripType("Work");
                        break;
                    case R.id.triptype_commute:
                        mTrip.setTripType("Commute");
                        break;
                    case R.id.triptype_personal:
                        mTrip.setTripType("Personal");
                        break;
                }
            }
        });

        //---  destination:
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mDestinationButton = (Button) v.findViewById(R.id.trip_dest);
        mDestinationButton.setEnabled(false);

        mViewLocation = (Button) v.findViewById(R.id.viewlocation);
        mViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = LocationDisplayActivity.newIntent(getActivity());
                intent.putExtra("Lat", mTrip.getTripLat());
                intent.putExtra("Lon", mTrip.getTripLong());
                startActivity(intent);
            }
        });

        //--- duration:
        durationInit(v);
        mMinsPicker.setValue(mTrip.getTripMins());
        mMinsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mTrip.setTripMins(newVal);
            }
        });

        mHrsPicker.setValue(mTrip.getTripHrs());
        mHrsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mTrip.setTripHrs(newVal);
            }
        });

        mDaysPicker.setValue(mTrip.getTripDays());
        mDaysPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mTrip.setTripDays(newVal);
            }
        });

        //--- Trip title:
        mCommentField = (EditText)v.findViewById(R.id.trip_comment);
        mCommentField.setText(mTrip.getComment());
        mCommentField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes trip title to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTrip.setComment(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });

        //--- Trip Image:
        mPhotoButton = (ImageButton) v.findViewById(R.id.trip_camera);
        PackageManager packageManager = getActivity().getPackageManager();

        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        boolean canTakePhoto = mPhotoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);

        if (canTakePhoto) {
            Uri uri = Uri.fromFile(mPhotoFile);
            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }

        mPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(captureImage, REQUEST_PHOTO);
            }
        });

        mPhotoView = (ImageView) v.findViewById(R.id.trip_photo);
        updatePhotoView();

        return v;
    }

    public void durationInit(View v){
        mMinsPicker = (NumberPicker) v.findViewById(R.id.pick_mins);
        mMinsPicker.setMaxValue(59);
        mMinsPicker.setMinValue(0);

        mHrsPicker = (NumberPicker) v.findViewById(R.id.pick_hrs);
        mHrsPicker.setMaxValue(23);
        mHrsPicker.setMinValue(0);

        mDaysPicker = (NumberPicker) v.findViewById(R.id.pick_days);
        mDaysPicker.setMaxValue(10);
        mDaysPicker.setMinValue(0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_PHOTO) {
            updatePhotoView();
        }
    }

        @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mTrip.getLocationDisp() == null) {
            Log.i("Check", "Location has not yet been set");
            if(mCurrentLocation == null){
                Log.i("Check", "Loc Servs unavailable");
                //set mtriplon to null
                //set mtriplocdisp to generic msg
                // set text of button to mtrip.getlocationdisp
                mTrip.setTripLat(null);
                mTrip.setTripLong(null);
                mTrip.setLocationDisp("Latitude: Unavailable"
                        + System.getProperty("line.separator")
                        + " Longitude: Unavailable");
                mDestinationButton.setText(mTrip.getLocationDisp());
            } else {
                Log.i("Check", "Loc Servs Available");
                //set lat lon disp to actuals
                // set text of button to mtrip.getlocationdisp
                mTrip.setTripLat(mCurrentLocation.getLatitude());
                mTrip.setTripLong(mCurrentLocation.getLongitude());
                mTrip.setLocationDisp("Latitude: " + mTrip.getTripLat()
                        + System.getProperty("line.separator")
                        + " Longitude: " + mTrip.getTripLong());
                mDestinationButton.setText(mTrip.getLocationDisp());
            }
        } else {
            Log.i("Check", "Location has already been set");
            mDestinationButton.setText(mTrip.getLocationDisp());
        }
    }
}
