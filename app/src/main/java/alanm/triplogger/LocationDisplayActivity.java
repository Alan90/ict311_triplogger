package alanm.triplogger;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by adm011 on 4/10/2016.
 */
public class LocationDisplayActivity extends SingleFragmentActivity {

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, LocationDisplayActivity.class);
        return intent;
    }

    /**
     * Method : override - createFragment()
     * Description : creates fragment
     * @return TripListFragment
     */
    @Override
    protected Fragment createFragment() { return new LocationDisplayFragment(); }
}
