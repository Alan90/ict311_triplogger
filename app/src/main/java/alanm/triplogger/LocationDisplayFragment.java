package alanm.triplogger;

import android.os.Bundle;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by adm011 on 4/10/2016.
 */
public class LocationDisplayFragment extends SupportMapFragment {
    private GoogleMap mMap;

    private double mThisLat;
    private double mThisLon;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mThisLat = (double) getActivity().getIntent().getSerializableExtra("Lat");
                mThisLon = (double) getActivity().getIntent().getSerializableExtra("Lon");

                updateUI();
            }
        });
    }

    private void updateUI(){
        if(mMap == null){
            return;
        }

        LatLng myPoint = new LatLng(mThisLat, mThisLon);

        MarkerOptions myMarker = new MarkerOptions().position(myPoint);

        mMap.clear();
        mMap.addMarker(myMarker);

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(myPoint)
                .build();

        int margin = 100;
        CameraUpdate update = CameraUpdateFactory.newLatLngBounds(bounds, margin);
        mMap.animateCamera(update);
    }
}
