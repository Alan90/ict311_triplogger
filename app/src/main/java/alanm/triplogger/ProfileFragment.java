package alanm.triplogger;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileFragment extends Fragment {
    private static final String ARG_PROFILE_ID = "profile_id";
    private Profile mProfile;

    private EditText mStudentNameField;
    private EditText mStudentIDField;
    private EditText mEmail;
    private EditText mComment;

    private RadioButton mGenderMale;
    private RadioButton mGenderFemale;
    private RadioButton mGenderOther;
    private RadioGroup mGenderGroup;

    /**
     * Method : newInstance(UUID)
     * Description : instantiates a new ProfileFragment instance
     * @return : (ProfileFragment) fragment
     */
    public static ProfileFragment newInstance(){
        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Method : override - onCreate()
     * Description : initialises fragment
     * @param savedInstanceState : state of the saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        mProfile = ProfileRepo.get(getActivity()).getProfile();
    }

    /**
     * Method : override - onPause()
     * Description : re-initialises fragment
     */
    @Override
    public void onPause() {
        super.onPause();

        ProfileRepo.get(getActivity())
                .updateProfile(mProfile);
    }

    /**
     * Method: override - onCreateView()
     * Description : used to create ui elements and generate the UI file
     *
     * @param inflater : use to inflate layout
     * @param container : container to hold the fragment
     * @param savedInstanceState : saved instance state
     * @return : (View) v
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_profile, container,false);

        //--- Student Name:
        mStudentNameField = (EditText)v.findViewById(R.id.settings_studentname);
        mStudentNameField.setText(mProfile.getStudentName());
        mStudentNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes student name to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mProfile.setStudentName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });

        //--- Student ID:
        mStudentIDField = (EditText)v.findViewById(R.id.settings_studentid);
        mStudentIDField.setText(mProfile.getStudentId());
        mStudentIDField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes student id to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mProfile.setStudentId(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });
        //--- Email:
        mEmail = (EditText)v.findViewById(R.id.settings_studentemail);
        mEmail.setText(mProfile.getEmail());
        mEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes email to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mProfile.setEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });

        //--- Gender:
        mGenderMale = (RadioButton) v.findViewById(R.id.studentgender_male);
        mGenderFemale = (RadioButton) v.findViewById(R.id.studentgender_female);
        mGenderOther = (RadioButton) v.findViewById(R.id.studentgender_other);
        mGenderGroup = (RadioGroup) v.findViewById(R.id.studentgender_group);

        String studentGenderCurrent = mProfile.getGender();

        if(studentGenderCurrent != null){
            switch(studentGenderCurrent){
                case "Male":
                    mGenderMale.setChecked(true);
                    break;
                case "Female":
                    mGenderFemale.setChecked(true);
                    break;
                case "Other":
                    mGenderOther.setChecked(true);
                    break;
            }
        }
        else{
            //Set default value:
            mProfile.setGender("Male");
            mGenderMale.setChecked(true);
        }

        mGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.studentgender_male:
                        mProfile.setGender("Male");
                        break;
                    case R.id.studentgender_female:
                        mProfile.setGender("Female");
                        break;
                    case R.id.studentgender_other:
                        mProfile.setGender("Other");
                        break;
                }
            }
        });

        //--- Comment:
        mComment = (EditText)v.findViewById(R.id.settings_comment);
        mComment.setText(mProfile.getComment());
        mComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Intentionally left blank
            }

            @Override
            //Changes comment to new value
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mProfile.setComment(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Intentionally left blank
            }
        });

        return v;
    }

}
