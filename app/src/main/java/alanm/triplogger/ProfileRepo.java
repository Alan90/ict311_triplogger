package alanm.triplogger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import alanm.triplogger.database.ProfileBaseHelper;
import alanm.triplogger.database.ProfileCursorWrapper;
import alanm.triplogger.database.ProfileDbSchema.ProfileTable;

/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileRepo {
    private static ProfileRepo sProfileRepo;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    /**
     * Method : get()
     * Description : returns a new ProfileRepo
     * @param context
     * @return (ProfileRepo) sProfileRepo
     */
    public static ProfileRepo get(Context context){
        if (sProfileRepo == null) {
            sProfileRepo = new ProfileRepo(context);
        }
        return sProfileRepo;
    }

    /**
     * Method : ProfileRepo()
     * Description : creates new ProfileBaseHelper and runs getWritableDatabase()
     */
    private ProfileRepo(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new ProfileBaseHelper(mContext)
                .getWritableDatabase();
        for (int i = 0; i < 100; i++){
            Profile profile = new Profile();
            profile.setStudentName("Profile # " + i);
        }
    }

    /**
     * Method : addProfile()
     * Description : adds a new profile to the database
     * @param p
     */
    public void addProfile(Profile p){
        ContentValues values = getContentValues(p);

        mDatabase.insert(ProfileTable.NAME, null, values);
    }

    /**
     * Method : getProfiles()
     * Description : returns a list of Profiles
     * @return List Profiles
     */
    public List<Profile> getProfiles(){
        List<Profile> profiles = new ArrayList<>();

        ProfileCursorWrapper cursor = queryProfiles();

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                profiles.add(cursor.getProfile());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return profiles;
    }

    /**
     * Method : getProfile()
     * Description : returns Profile at given cursor
     * @return (ProfileCursorWrapper) cursor
     */
    public Profile getProfile(){
        ProfileCursorWrapper cursor = queryProfiles();

        try {
            if (cursor.getCount() == 0){
                return null;
            }

            cursor.moveToFirst();
            return cursor.getProfile();
        } finally {
            cursor.close();
        }
    }

    /**
     * Method : updateProfile()
     * Description : used to update the database
     */
    public void updateProfile(Profile profile) {
        String uuidString = profile.getId().toString();
        ContentValues values = getContentValues(profile);

        mDatabase.update(ProfileTable.NAME, values,
                null,
                null);
    }


    /**
     * Method : getContentValues()
     * Description : used to assist in writing and updating to the database
     * @return (ContentValues) values
     */
    private static ContentValues getContentValues(Profile profile){
        ContentValues values = new ContentValues();
        values.put(ProfileTable.Cols.STUDENTNAME, profile.getStudentName());
        values.put(ProfileTable.Cols.STUDENTID, profile.getStudentId());
        values.put(ProfileTable.Cols.GENDER, profile.getGender());
        values.put(ProfileTable.Cols.EMAIL, profile.getEmail());
        values.put(ProfileTable.Cols.COMMENT, profile.getComment());

        return values;
    }

    /**
     * Method : queryProfiles()
     * Description : querys the database
     * @return query
     */
    private ProfileCursorWrapper queryProfiles(){
        Cursor cursor = mDatabase.query(
                ProfileTable.NAME,
                null, // null select all columns
                null,
                null,
                null, //group by
                null, // having
                null // orderBy
        );

        return new ProfileCursorWrapper(cursor);
    }}