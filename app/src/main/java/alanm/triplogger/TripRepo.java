package alanm.triplogger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import alanm.triplogger.database.TripCursorWrapper;
import alanm.triplogger.database.TripDbSchema.TripTable;
import alanm.triplogger.database.TripBaseHelper;

/**
 * Created by alanm on 9/09/2016.
 *
 * Contains a list of all trips
 * Model level
 */

public class TripRepo {
    private static TripRepo sTripRepo;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    /**
     * Method : get()
     * Description : returns a new TripRepo
     * @param context
     * @return (TripRepo) sTripRepo
     */
    public static TripRepo get(Context context){
        if (sTripRepo == null) {
            sTripRepo = new TripRepo(context);
        }
        return sTripRepo;
    }

    /**
     * Method : TripRepo()
     * Description : creates new TripBaseHelper and runs getWritableDatabase()
     */
    private TripRepo(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new TripBaseHelper(mContext)
                .getWritableDatabase();
        for (int i = 0; i < 100; i++){
            Trip trip = new Trip();
            trip.setTitle("Trip #" + i);
        }
    }

    /**
     * Method : addTrip()
     * Description : adds a new trip to the database
     * @param t
     */
    public void addTrip(Trip t){
        ContentValues values = getContentValues(t);

        mDatabase.insert(TripTable.NAME, null, values);
    }

    /**
     * Method : getTrips()
     * Description : returns a list of trips
     * @return List trips
     */
    public List<Trip> getTrips(){
        List<Trip> trips = new ArrayList<>();

        TripCursorWrapper cursor =queryTrips(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                trips.add(cursor.getTrip());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return trips;
    }

    /**
     * Method : getTrip()
     * Description : returns trip at given cursor
     * @return (TripCursorWrapper) cursor
     */
    public Trip getTrip(UUID id){
        TripCursorWrapper cursor = queryTrips(
                TripTable.Cols.UUID + " = ?",
                new String[] { id.toString() }
        );

        try {
            if (cursor.getCount() == 0){
                return null;
            }

            cursor.moveToFirst();
            return cursor.getTrip();
        } finally {
            cursor.close();
        }
    }

    /**
     * Method : getPhotoFile()
     * Description : Finds where the photo lives
     */
    public File getPhotoFile(Trip trip){
        File externalFilesDir = mContext
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFilesDir == null){
            return null;
        }

        return new File(externalFilesDir, trip.getPhotoFilename());
    }

    /**
     * Method : updateTrip()
     * Description : used to update the database
     */
    public void updateTrip(Trip trip) {
        String uuidString = trip.getId().toString();
        ContentValues values = getContentValues(trip);

        mDatabase.update(TripTable.NAME, values,
                TripTable.Cols.UUID + " = ?",
                new String[] { uuidString });
    }

    /**
     * Method : deleteTrip()
     * Description : deletes record from the database
     */
    public void deleteTrip(Trip trip){
        String uuidString = trip.getId().toString();

        mDatabase.delete(TripTable.NAME, TripTable.Cols.UUID + " = ? ",
                            new String[] { uuidString });
    }

    /**
     * Method : getContentValues()
     * Description : used to assist in writing and updating to the database
     * @return (ContentValues) values
     */
    private static ContentValues getContentValues(Trip trip){
        ContentValues values = new ContentValues();
        values.put(TripTable.Cols.UUID, trip.getId().toString());
        values.put(TripTable.Cols.TITLE, trip.getTitle());
        values.put(TripTable.Cols.DATE, trip.getDate().getTime());
        values.put(TripTable.Cols.TRIPTYPE, trip.getTripType());
        values.put(TripTable.Cols.TRIPLAT, trip.getTripLat());
        values.put(TripTable.Cols.TRIPLONG, trip.getTripLong());
        values.put(TripTable.Cols.TRIPLOCDISP, trip.getLocationDisp());
        values.put(TripTable.Cols.TRIPDAYS, trip.getTripDays());
        values.put(TripTable.Cols.TRIPHRS, trip.getTripHrs());
        values.put(TripTable.Cols.TRIPMINS, trip.getTripMins());
        values.put(TripTable.Cols.TRIPCOMMENT, trip.getComment());

        return values;
    }

    /**
     * Method : queryTrips()
     * Description : querys the database
     * @return query
     */
    private TripCursorWrapper queryTrips(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                TripTable.NAME,
                null, // null select all columns
                whereClause,
                whereArgs,
                null, //group by
                null, // having
                null // orderBy
        );

        return new TripCursorWrapper(cursor);
    }}
