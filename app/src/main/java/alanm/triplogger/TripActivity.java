package alanm.triplogger;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import java.util.UUID;

/**
 * Created by alanm on 9/09/2016.
 *
 * This class is the activity to host the trip fragment, which contains details of a created trip
 */

public class TripActivity extends SingleFragmentActivity {

    private static final String EXTRA_TRIP_ID =
            "alanm.triplogger.trip_id";

    public static Intent newIntent(Context packageContext, UUID tripId) {
        Intent intent = new Intent(packageContext, TripActivity.class);
        intent.putExtra(EXTRA_TRIP_ID, tripId);
        return intent;
    }

    @Override
    protected Fragment createFragment() {

        UUID tripId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_TRIP_ID);
        return TripFragment.newInstance(tripId);
    }

}
