package alanm.triplogger;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
/**
 * Created by alanm on 6/10/2016.
 */
public class ProfileActivity extends SingleFragmentActivity {


    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, ProfileActivity.class);
        return intent;
    }

    @Override
    protected Fragment createFragment() {


        return ProfileFragment.newInstance();
    }
}
